create table users
(
    id bigint auto_increment,
    constraint users_pk
        primary key (id),
    first_name TEXT not null,
    last_name TEXT not null,
    age int null,
    email TEXT not null unique,
    points int null


);
create table films
(
    id bigint auto_increment,
    constraint films_pk
        primary key (id),
    title TEXT not null,
    director TEXT not null,
    note float null,
    date_sortie date null,
    synopsis TEXT null,
    image TEXT null
);

create table avis
(
    id bigint auto_increment,
    constraint avis_pk
        primary key (id),
    texte TEXT not null,
    note int null,
    film_id int null,
    user_id int null,
    date_avis date null

);