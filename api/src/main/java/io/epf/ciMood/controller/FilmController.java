package io.epf.ciMood.controller;

import io.epf.ciMood.DAO.FilmDAO;
import io.epf.ciMood.model.Film;
import io.epf.ciMood.model.StorageService;
import org.springframework.http.*;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/films")
@CrossOrigin
public class FilmController {

    private final FilmDAO filmDAO;
    private final StorageService storageService;

    public FilmController(FilmDAO filmDAO, StorageService storageService) {
        this.filmDAO = filmDAO;
        this.storageService = storageService;
    }

    /**
     * Liste tous les films
     *
     * @return
     */
    @GetMapping()
    public List<Film> getFilms() {
        Iterable<Film> it = this.filmDAO.findAll();
        List<Film> films = new ArrayList<>();
        it.forEach(films::add);
        return films;
    }

    /**
     * Récupère un film par son id
     *
     * @param id
     * @return
     */
    @GetMapping("/film/{id}")
    public Film getFilmById(@PathVariable long id) {
        Film film = this.filmDAO.findById(id).orElseThrow(() -> new NoSuchElementException("Film non existent"));
        return film;
    }

    /**
     * @param filmId
     * @return l'image correspondant à un film
     */
    @GetMapping(value = "images/{filmId}", produces = MediaType.ALL_VALUE)
    public ResponseEntity<byte[]> getImage(@PathVariable Long filmId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentDisposition(ContentDisposition.builder("attachement").filename("filename.png").build());
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        return this.filmDAO.findById(filmId)
                .map(Film::getImage)
                .map(storageService::load)
                .map(bytes -> new ResponseEntity<>(bytes, headers, HttpStatus.OK))
                .orElse(null);
    }

    /**
     * @return la moyenne de satisfaction client soit la moyenne de tous les films
     */
    @GetMapping("/average")
    public Integer getAverageFilms() {
        List<Film> films = this.getFilms();
        int somme = 0, moy, total = 0, noteMaximale = 5;

        if (films != null) {
            for (int i = 0; i < films.size(); i++) {
                if (films.get(i).getNote() != null) {
                    somme += films.get(i).getNote();
                    total += 1;
                }
            }
            // trouver la valeur moyenne
            if (total != 0) {
                moy = somme / total;
            } else moy = 0;
            moy = moy * 100 / noteMaximale;
            return moy;
        } else return null;
    }

    /**
     * ajoute une image à un film et stocke l'image
     *
     * @param file
     * @param id
     */
    @PostMapping("/{id}")
    public void addFile(@RequestParam("filmImage") MultipartFile file, @PathVariable Long id) {
        Film film = this.filmDAO.findById(id).orElseThrow(() -> new NoSuchElementException("Film non existent"));
        String extension = StringUtils.getFilenameExtension(file.getOriginalFilename());
        if (film.getImage() != null) {
            storageService.delete(film.getImage());
        }
        film.setImage("image_film_" + id + "." + extension);
        this.filmDAO.save(film);
        storageService.store(file, "image_film_" + id + "." + extension);
    }

    /**
     * supprime l'image d'un film
     *
     * @param id
     */
    @PostMapping("/deleteFile")
    public void deleteFile(@RequestBody String id) {
        long filmId = Long.parseLong(id);
        Film film = this.filmDAO.findById(filmId).orElseThrow(() -> new NoSuchElementException("Film non existent"));
        storageService.delete(film.getImage());
        film.setImage(null);
        this.filmDAO.save(film);
    }

    /**
     * Ajoute un film
     *
     * @param film
     * @return
     */
    @PostMapping()
    public Film addFilm(@RequestBody Film film) {
        return this.filmDAO.save(film);
    }

    /**
     * Modifie les données d'un film
     *
     * @param newFilm
     * @return
     */
    @PutMapping("/film")
    public Film updateFilm(@RequestBody Film newFilm) {
        Long id = newFilm.getId();
        return this.filmDAO.findById(id)
                .map(film -> {
                    film.setTitle(newFilm.getTitle());
                    film.setDirector(newFilm.getDirector());
                    film.setDateSortie(newFilm.getDateSortie());
                    film.setSynopsis(newFilm.getSynopsis());
                    return this.filmDAO.save(film);
                })
                .orElseGet(() -> {
                    newFilm.setId(id);
                    return this.filmDAO.save(newFilm);
                });
    }

    /**
     * Supprime un film par son id
     *
     * @param id
     */
    @DeleteMapping("/{id}")
    public void deleteFilm(@PathVariable Long id) {
        this.filmDAO.deleteById(id);
    }

    /**
     * @param id du User
     * @return tous les films pour lesquels l'utilisateur a donné un avis
     */
    @GetMapping("/userfilms/{id}")
    public List<Film> getFilmsByUserId(@PathVariable int id) {
        Iterable<Film> it = this.filmDAO.getFilmsByUserId(id);
        List<Film> films = new ArrayList<>();
        it.forEach(films::add);
        return films;
    }
}
