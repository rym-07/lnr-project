package io.epf.ciMood.controller;

import io.epf.ciMood.DAO.UserDAO;
import io.epf.ciMood.model.Avis;
import io.epf.ciMood.model.Film;
import io.epf.ciMood.model.User;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/users")
@CrossOrigin
public class UserController {

    private final UserDAO userDAO;

    public UserController(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    /**
     * Liste tous les utilisateurs
     *
     * @return
     */
    @GetMapping()
    public List<User> getUsers() {
        Iterable<User> it = this.userDAO.findAll();
        List<User> users = new ArrayList<>();
        it.forEach(e -> users.add(e));
        return users;
    }

    /**
     * Récupère un utilisateur en fonction de son email
     *
     * @param email
     * @return
     */
    @GetMapping("/{email}")
    public User getUserByEmail(@PathVariable String email) {
        Optional<User> user = userDAO.findByEmail(email);
        return user.orElse(null);
    }

    /**
     * Recupère un utilisateur en fonction de son id
     *
     * @param id
     * @return
     */
    @GetMapping("/user/{id}")
    public User getUserById(@PathVariable long id) {
        User user = this.userDAO.findById(id).orElseThrow(() -> new NoSuchElementException("User non existent"));
        return user;
    }

    /**
     * Ajoute un utilisateur en initialisant ses points de fidélité à 0
     *
     * @param user
     * @return
     */
    @PostMapping()
    public User addUser(@RequestBody User user) {
        user.setPoints(0);
        return this.userDAO.save(user);
    }

    /**
     * Supprime un User par son id
     *
     * @param id
     */
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        this.userDAO.deleteById(id);
    }

    /**
     * Modifie les données d'un user
     * @param newUser
     * @return
     */
    @PutMapping("/user")
    public User updateUser(@RequestBody User newUser) {
        Long id = newUser.getId();
        return this.userDAO.findById(id)
                .map(user -> {
                    user.setFirstName(newUser.getFirstName());
                    user.setLastName(newUser.getLastName());
                    user.setEmail(newUser.getEmail());
                    user.setAge(newUser.getAge());
                    return this.userDAO.save(user);
                })
                .orElseGet(() -> {
                    newUser.setId(id);
                    return this.userDAO.save(newUser);
                });
    }

}


