package io.epf.ciMood.controller;

import io.epf.ciMood.DAO.AvisDAO;
import io.epf.ciMood.DAO.FilmDAO;
import io.epf.ciMood.DAO.UserDAO;
import io.epf.ciMood.model.Avis;
import io.epf.ciMood.model.Film;
import io.epf.ciMood.model.CheckAvisDTO;
import io.epf.ciMood.model.User;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
@RequestMapping("/avis")
@CrossOrigin
public class AvisController {

    private final AvisDAO avisDAO;
    private final FilmDAO filmDAO;
    private final UserDAO userDAO;


    public AvisController(AvisDAO avisDAO, FilmDAO filmDAO, UserDAO userDAO) {
        this.avisDAO = avisDAO;
        this.filmDAO = filmDAO;
        this.userDAO = userDAO;
    }

    /**
     * @return retourne une liste de tous les avis
     */
    @GetMapping()
    public List<Avis> getAvis() {
        Iterable<Avis> it = this.avisDAO.findAll();
        List<Avis> avis = new ArrayList<>();
        it.forEach(avis::add);
        return avis;
    }

    /**
     * Méthode get
     *
     * @param id
     * @return un avis en fonction de son id
     */
    @GetMapping("/this/{id}")
    public Avis getAvisById(@PathVariable long id) {
        Avis avis = this.avisDAO.findById(id).orElseThrow(() -> new NoSuchElementException("Avis non existent"));
        return avis;
    }

    /**
     * @param year
     * @return un tableau de longueur 12, avec le nb d'avis par mois pour une année
     */
    @GetMapping({"/{year}"})
    public List<Integer> findNbAvisByYear(@PathVariable String year) {

        Integer nbMois = 12;
        List<Integer> nbAvisParMois = new ArrayList<>();
        for (int i = 1; i <= nbMois; i++) {
            String mois = Integer.toString(i);
            Integer it = this.avisDAO.findNbAvisByYear(year, mois);
            nbAvisParMois.add(it);
        }
        return nbAvisParMois;
    }

    /**
     * @param year
     * @return tous les avis d'une année précise
     */
    @GetMapping({"findAvisBy/{year}"})
    public List<Avis> findAvisByYear(@PathVariable String year) {
        Iterable<Avis> it = this.avisDAO.findAvisByYear(year);
        List<Avis> avisList = new ArrayList<>();
        it.forEach(avisList::add);
        return avisList;
    }

    /**
     * A l'ajout d'un avis on update également la note du film concerné et
     * le nombre de points de fidélité du User auteur de l'ajout de l'avis
     *
     * @param avis
     * @return un avis qui vient d'être ajouté
     */
    @PostMapping()
    public Avis addAvis(@RequestBody Avis avis) {
        this.avisDAO.save(avis);
        Float noteFilm = this.avisDAO.AverageFilmNoteByAvis(avis.getFilm().getId());
        Film film = avis.getFilm();

        try {
            User user = avis.getUser();
            int points = (user.getPoints()) + 2;
            this.filmDAO.updateNoteFilm(noteFilm, film.getId());
            this.userDAO.updatePtsFidelites(points, user.getId());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        Date date = new Date();
        avis.setDateAvis(date);
        return this.avisDAO.save(avis);
    }

    /**
     * Modifie un avis en mettant à jour la note du film concernée si la modification
     * de l'avis concerne la note donnée
     *
     * @param newAvis
     * @return
     */
    @PutMapping("/avis")
    public Avis updateAvis(@RequestBody Avis newAvis) {
        Long id = newAvis.getId();
        return this.avisDAO.findById(id)
                .map(avis -> {
                    avis.setTexte(newAvis.getTexte());
                    Date date = new Date();
                    avis.setDateAvis(date);
                    avis.setUser(newAvis.getUser());
                    avis.setFilm(newAvis.getFilm());
                    avis.setNote(newAvis.getNote());
                    this.avisDAO.save(avis);
                    Float newNote = this.avisDAO.AverageFilmNoteByAvis(newAvis.getFilm().getId());
                    Film film = avis.getFilm();
                    try {
                        this.filmDAO.updateNoteFilm(newNote, film.getId());
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    return this.avisDAO.save(avis);
                })
                .orElseGet(() -> {
                    newAvis.setId(id);
                    return this.avisDAO.save(newAvis);
                });
    }

    /**
     * Supprime un avis par son id et change la note du film correspondant
     *
     * @param id
     */
    @DeleteMapping("/{id}")
    public void deleteAvis(@PathVariable Long id) {
        Avis avis = this.avisDAO.findById(id).orElseThrow(() -> new NoSuchElementException("Avis non existent"));
        Film film = avis.getFilm();
        this.avisDAO.deleteById(id);
        int nbAvis = this.avisDAO.findNbAvisByFilmId(film.getId());
        if (nbAvis > 0) {
            Float noteFilm = this.avisDAO.AverageFilmNoteByAvis(film.getId());
            this.filmDAO.updateNoteFilm(noteFilm, film.getId());
        } else this.filmDAO.updateNoteFilmNull(film.getId());


    }

    /**
     * @param id du User
     * @return la liste des avis pour un utilisateur donné
     */
    @GetMapping("/user/{id}")
    public List<Avis> getAvisByUserId(@PathVariable int id) {
        Iterable<Avis> it = this.avisDAO.getAvisByUserId(id);
        List<Avis> avisList = new ArrayList<>();
        it.forEach(avisList::add);
        return avisList;
    }

    /**
     * @param filmId
     * @return la liste des avis pour un film donné
     */
    @GetMapping("/film/{filmId}")
    public List<Avis> getAvisByFilm(@PathVariable String filmId) {
        long id = Long.parseLong(filmId);
        Iterable<Avis> it = this.avisDAO.findAvisByFilmId(id);
        List<Avis> avis = new ArrayList<>();
        it.forEach(avis::add);
        return avis;
    }

    /**
     * @param checkAvis
     * @return si un user a deja mis un avis pour un film
     */
    @GetMapping("/checkAvis")
    public boolean checkAddAvis(CheckAvisDTO checkAvis) {
        String userId = checkAvis.getUserId();
        String filmId = checkAvis.getFilmId();
        int nbsAvis = this.avisDAO.countAvisByUserAndFilm(userId, filmId);
        return nbsAvis < 1;
    }

    /**
     * Méthode get
     *
     * @return toutes les années de publication d'avis
     */
    @GetMapping("/findAllYears")
    public List<Integer> findAllYears() {
        List<Integer> years = this.avisDAO.findAllYearsAvis();
        return years;

    }
}
