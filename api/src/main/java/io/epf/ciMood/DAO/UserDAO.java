package io.epf.ciMood.DAO;

import io.epf.ciMood.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UserDAO extends CrudRepository<User, Long> {
    /**
     *
     * @param email
     * @return un user en fonction de son email
     */
    Optional<User> findByEmail(String email);

    /**
     * Update des points de fidélités d'un user
     * @param pts
     * @param userId
     */
    @Query(value = "update users u set u.points = ?1 where u.id = ?2",
            nativeQuery = true)
    void updatePtsFidelites(int pts, long userId);
}
