package io.epf.ciMood.DAO;

import io.epf.ciMood.model.Film;
import io.epf.ciMood.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FilmDAO extends CrudRepository<Film, Long> {
    /**
     * @param userId
     * @return tous les films pour lesquels un user a laissé un avis
     */
    @Query(value = "SELECT distinct f.id, f.title, f.director, f.note, f.date_sortie, f.synopsis, f.image  FROM avis a INNER JOIN films f on a.film_id = f.id WHERE user_id = ?1",
            nativeQuery = true)
    Iterable<Film> getFilmsByUserId(long userId);

    /**
     * Update de la note du film avec une valuer passée en parametre
     *
     * @param note
     * @param filmId
     */
    @Query(value = "update films f set f.note = ?1 where f.id = ?2",
            nativeQuery = true)
    void updateNoteFilm(float note, long filmId);

    /**
     * Update de la note du film à null
     *
     * @param filmId
     */
    @Query(value = "update films f set f.note = null where f.id = ?1",
            nativeQuery = true)
    void updateNoteFilmNull(long filmId);
}

