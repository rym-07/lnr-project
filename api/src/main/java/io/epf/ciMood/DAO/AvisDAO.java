package io.epf.ciMood.DAO;

import io.epf.ciMood.model.Avis;
import io.epf.ciMood.model.Film;
import io.epf.ciMood.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.time.Year;
import java.util.List;

import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

@Repository
public interface AvisDAO extends CrudRepository<Avis, Long> {
    /**
     * @param film_id
     * @return un avis en fonction du film indiqué
     */
    @Query(value = "SELECT * FROM avis WHERE film_id= ?1", nativeQuery = true)
    Iterable<Avis> findAvisByFilmId(Long film_id);

    /**
     * @param film_id
     * @return le nombre d'avis d'un film
     */
    @Query(value = "SELECT COUNT(*) FROM avis WHERE film_id= ?1", nativeQuery = true)
    Integer findNbAvisByFilmId(Long film_id);

    /**
     * @param year
     * @param month
     * @return le nombre d'avis pour un mois et une année donnée
     */
    @Query(value = "SELECT COUNT(*) FROM avis a WHERE YEAR(date_avis) = ?1 and MONTH(date_avis)= ?2",
            nativeQuery = true)
    Integer findNbAvisByYear(String year, String month);

    /**
     * @param year
     * @return les avis d'une année donnée
     */
    @Query(value = "SELECT * FROM avis a WHERE YEAR(date_avis) = ?1",
            nativeQuery = true)
    Iterable<Avis> findAvisByYear(String year);

    /**
     * @param userId
     * @return la liste des avis pour un user donné
     */
    @Query(value = "SELECT a.id, a.texte, a.note, a.film_id, a.user_id, a.date_avis FROM avis a WHERE user_id = ?1",
            nativeQuery = true)
    Iterable<Avis> getAvisByUserId(long userId);

    /**
     * @param filmId
     * @return la note moyenne des avis d'un film donné
     */
    @Query(value = "SELECT AVG(note) FROM avis WHERE film_id=?", nativeQuery = true)
    Float AverageFilmNoteByAvis(Long filmId);

    /**
     * @return toutes les années de publication des avis
     */
    @Query(value = "SELECT DISTINCT YEAR(date_avis) FROM avis ORDER BY YEAR(date_avis) DESC", nativeQuery = true)
    List<Integer> findAllYearsAvis();

    /**
     * @param userId
     * @param FilmId
     * @return le nombre d'avis pour un user et un film donnés
     */
    @Query(value = "SELECT COUNT(*) FROM avis WHERE user_id= ?1 and film_id = ?2",
            nativeQuery = true)
    Integer countAvisByUserAndFilm(String userId, String FilmId);
}
