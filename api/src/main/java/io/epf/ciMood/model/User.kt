package io.epf.ciMood.model

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*


@Entity(name = "users")
data class User(
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id var id: Long?,
        @OneToMany(mappedBy = "user", orphanRemoval = true, cascade = [CascadeType.REMOVE]) @JsonIgnore var avis: MutableList<Avis> = mutableListOf(),
        @Column(name = "first_name") var firstName: String?,
        @Column(name = "last_name") var lastName: String?,
        @Column(name = "age") var age: Int?,
        @Column(name = "email") var email: String?,
        @Column(name = "points") var points: Int?,
) {
    constructor() : this(null, mutableListOf(), null, null, null, null, 0)

}


