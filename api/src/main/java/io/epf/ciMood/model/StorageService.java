package io.epf.ciMood.model;

import org.springframework.web.multipart.MultipartFile;

/**
 * Service permettant le chargement, le stockage et la suppresion de fichier
 */
public interface StorageService {

    void store(MultipartFile file, String name);

    void delete(String name);

    byte[] load(String filename);
}
