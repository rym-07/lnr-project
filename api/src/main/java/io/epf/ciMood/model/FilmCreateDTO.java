package io.epf.ciMood.model;


import org.springframework.web.multipart.MultipartFile;

/**
 * DTO pour récupérer une requète contenant un film et un fichier
 */
public class FilmCreateDTO {
    private Film film;
    private MultipartFile filmImage;

    public FilmCreateDTO() {
    }

    public FilmCreateDTO(Film film, MultipartFile filmImage) {
        this.film = film;
        this.filmImage = filmImage;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public MultipartFile getFilmImage() {
        return filmImage;
    }

    public void setFilmImage(MultipartFile filmImage) {
        this.filmImage = filmImage;
    }
}

