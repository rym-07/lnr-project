package io.epf.ciMood.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity(name = "films")
public class Film {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    @OneToMany(mappedBy = "film", orphanRemoval = true, cascade = CascadeType.REMOVE)
    @JsonIgnore
    private List<Avis> avis;
    @Column(name = "title")
    private String title;
    @Column(name = "director")
    private String director;
    @Column(name = "note")
    private Float note;
    @Column(name = "date_sortie")
    private Date dateSortie;
    @Column(name = "synopsis")
    private String synopsis;
    @Column(name = "image")
    private String image;

    public Film(Long id, String title, String director, Float note, Date dateSortie, String synopsis) {
        this.id = id;
        this.title = title;
        this.director = director;
        this.note = note;
        this.dateSortie = dateSortie;
        this.synopsis = synopsis;
    }

    public Film() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setNote(Float note) {
        this.note = note;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDirector() {
        return director;
    }

    public Float getNote() {
        return note;
    }

    public Date getDateSortie() {
        return dateSortie;
    }

    public void setDateSortie(Date dateSortie) {
        this.dateSortie = dateSortie;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {

        this.image = image;
    }

    @Override
    public String toString() {
        return "Film{" +
                "id=" + id +
                ", avis=" + avis +
                ", title='" + title + '\'' +
                ", director='" + director + '\'' +
                ", note=" + note +
                ", dateSortie=" + dateSortie +
                ", synopsis='" + synopsis + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
