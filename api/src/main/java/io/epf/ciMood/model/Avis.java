package io.epf.ciMood.model;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
public class Avis {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
    @ManyToOne
    @JoinColumn(name = "film_id", nullable = false)
    private Film film;
    @Column(name = "note")
    private Integer note;
    @Column(name = "texte")
    private String texte;
    @Column(name = "date_avis")
    private Date dateAvis;

    public Avis(Long id, User user, Film film, Integer note, String texte, Date dateAvis) {
        this.id = id;
        this.user = user;
        this.film = film;
        this.note = note;
        this.texte = texte;
        this.dateAvis = new Date();
    }

    public Avis() {

    }

    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }

    public Date getDateAvis() {
        return dateAvis;
    }

    public void setDateAvis(Date dateAvis) {
        this.dateAvis = dateAvis;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public void setNote(Integer note) {
        this.note = note;
    }

    public User getUser() {
        return user;
    }

    public Film getFilm() {
        return film;
    }

    public Integer getNote() {
        return note;
    }
}
