package io.epf.ciMood.model;

/**
 * DTO pour récupérer des requetes contenant un userID et un filmID
 */
public class CheckAvisDTO {
    private String userId;
    private String filmId;

    public CheckAvisDTO() {
    }

    public CheckAvisDTO(String userId, String filmId) {
        this.userId = userId;
        this.filmId = filmId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFilmId() {
        return filmId;
    }

    public void setFilmId(String filmId) {
        this.filmId = filmId;
    }
}
