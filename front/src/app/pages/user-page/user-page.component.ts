import {Component, OnInit} from '@angular/core';
import {FilmService} from '../../services/film.service';
import {Film} from '../../models/film.model';
import {Avis} from '../../models/avis.model';
import {ActivatedRoute, Router} from '@angular/router';
import {AvisService} from '../../services/avis.service';
import {ConnectionService} from '../../services/connection.service';
import {NgForm} from '@angular/forms';
import {User} from '../../models/user.model';
import {UserService} from '../../services/user.service';

@Component({
    selector: 'app-user-page',
    templateUrl: './user-page.component.html',
    styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {

    constructor(private route: ActivatedRoute,
                private filmService: FilmService,
                private avisService: AvisService,
                private userService: UserService,
                private connectionService: ConnectionService,
                private router: Router) {
    }

    films: Film[];
    avisList: Avis[]
    userId: string;
    user: User

    // on recupère le user connecté, ses films et ses avis
    ngOnInit(): void {
        if (this.connectionService.user) {
            this.userService.getUserById(this.connectionService.user.id).subscribe(u => this.user = u)
            this.userId = this.connectionService.user.id.toString();
            this.filmService.getFilmsByUserId(this.userId).subscribe(films => this.films = films);
            this.avisService.getAvisByUserId(this.userId).subscribe(avisList => this.avisList = avisList);
        } else {
            this.router.navigateByUrl('/connection')
        }
    }

    redirectAvis(idUser: string) {
        this.router.navigateByUrl('/list-avis/' + idUser);
    }

    nouvelAvis() {
        this.router.navigateByUrl('/add-avis');
    }

    deconnexion() {
        this.connectionService.user = null
        this.router.navigateByUrl('/');
    }

    redirectInfos() {
        this.router.navigateByUrl('/add-user/' + this.connectionService.user.id);
    }

}
