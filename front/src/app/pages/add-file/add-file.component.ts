import {Component, OnInit} from '@angular/core';
import {FilmCreateDto, FilmService} from '../../services/film.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Film} from '../../models/film.model';

@Component({
    selector: 'app-add-file',
    templateUrl: './add-file.component.html',
    styleUrls: ['./add-file.component.css']
})
export class AddFileComponent implements OnInit {

    constructor(private filmService: FilmService, private route: ActivatedRoute, private router: Router) {
    }

    selectedFile: File;
    id: string;
    film: Film

    // à l'initialisation de la page, on recupère le film auquel il faut ajouter une image
    ngOnInit(): void {
        this.id = this.route.snapshot.paramMap.get('id')
        this.filmService.getFilmById(parseInt(this.id, 10)).subscribe(f => {
            this.film = f
        })
    }

    // quand le file est précisé on le récupère
    public onFileChanged(event) {
        // Select File
        this.selectedFile = event.target.files.item(0);
    }

    // quand le file est uploader on l'envoie au back
    public onUpload() {
        const filmCreateDto: FilmCreateDto = {
            id: this.id,
            filmImage: this.selectedFile
        }

        this.filmService.addFile(filmCreateDto).subscribe(f => console.log(filmCreateDto));
        this.router.navigateByUrl('/list-films');
    }

    // suppression d'un file lors d'un clic sur le bouton supprimer
    public onDelete() {
        this.filmService.deleteFile(this.id).subscribe(succes => {
            console.log('delete')
        })
        setTimeout(() => {
            this.router.navigateByUrl('/list-films')
        }, 1000);
    }

}
