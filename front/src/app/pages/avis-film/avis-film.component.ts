import { Component, OnInit } from '@angular/core';
import {AvisService} from '../../services/avis.service';
import {Avis} from '../../models/avis.model';
import {ActivatedRoute, Router} from '@angular/router';
import {Film} from '../../models/film.model';
import {FilmService} from '../../services/film.service';

@Component({
  selector: 'app-avis-film',
  templateUrl: './avis-film.component.html',
  styleUrls: ['./avis-film.component.css']
})
export class AvisFilmComponent implements OnInit {
  avis: Avis[];
  films: Film[];
  id:string
  constructor(private avisService: AvisService ,
              private filmService: FilmService,
              private route: ActivatedRoute,private router: Router) { }

  // A l'initialisation, on récupère tous les avis pour un film donné grâce à son id
  ngOnInit(): void {
    this.id=this.route.snapshot.paramMap.get('id');
    this.avisService.getAvisByFilm(this.id).subscribe(avis => this.avis=avis);
    this.filmService.getFilms().subscribe(film => this.films=film);
  }

}
