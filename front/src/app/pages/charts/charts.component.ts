import {AfterContentInit, Component, OnInit} from '@angular/core';


import {FilmService} from '../../services/film.service';
import {Film} from '../../models/film.model';
import {Chart, registerables} from 'chart.js';
import {ActivatedRoute, Router} from '@angular/router';
import {AvisService} from '../../services/avis.service';


@Component({
    selector: 'app-charts',
    templateUrl: './charts.component.html',
    styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {

    constructor(private filmService: FilmService, private avisService: AvisService, private router: Router, private route: ActivatedRoute) {
        Chart.register(...registerables);
    }

    films: Film[];
    canvas: any;
    chart: Chart;
    chart2: Chart;
    years: number[];
    yearSelected: string;

    // A l'initialisation,
    ngOnInit(): void {
        // on récupère toutes les années des avis
        this.avisService.findAllyears().subscribe(y => this.years = y)
        this.yearSelected = '2021'
        // on récupère tous les films, on affiche la note de tous les films sur un graphique
        this.filmService.getFilms().subscribe(films => {
            const ids = films.map(film => film.title);
            const notes = films.map(film => film.note);

            this.chart = new Chart('canvas0', {
                type: 'line',
                data: {
                    labels: ids,
                    datasets: [
                        {
                            data: notes,
                            borderColor: '#FECC00',
                        },
                    ]
                },
                options: {
                    responsive: true,
                    plugins: {
                        title: {
                            display: true,
                            text: 'Note par film'
                        },
                        legend: {display: false},
                    }
                }
            });
        });
        // on récupère le nombre d'avis par mois de chaque année. On affiche le nbs d'avis par mois de l'année selectionnée
        this.avisService.findNbAvisByYear(this.yearSelected).subscribe(nbAvisMois => {
            const mois = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'décembre'];
            this.chart2 = new Chart('canvas1', {
                type: 'bar',
                data: {
                    labels: mois,
                    datasets: [
                        {
                            data: nbAvisMois,
                            borderColor: '#FECC00',
                            backgroundColor: 'rgba(254,204,0,0.51)',
                        },
                    ]
                },
                options: {
                    responsive: true,
                    plugins: {
                        title: {
                            display: true,
                            text: 'Nombre d\'avis par année'
                        },
                        legend: {display: false},
                    }
                }
            })
        })
    }

    // lorsque l'année selectionnée est modifiée, on supprime l'ancien graphique et on en affiche un nouveau avec la bonne année
    valueSelectedChange(newValueYear) {
        this.yearSelected = newValueYear;
        this.avisService.findNbAvisByYear(newValueYear).subscribe(nbAvisMois => {
            if (this.chart2) this.chart2.destroy();
            const mois = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'décembre'];

            this.chart2 = new Chart('canvas1', {
                type: 'bar',
                data: {
                    labels: mois,
                    datasets: [
                        {
                            borderColor: '#FECC00',
                            backgroundColor: 'rgba(254,204,0,0.51)',
                            data: nbAvisMois,
                        },
                    ]
                },
                options: {
                    responsive: true,
                    plugins: {
                        title: {
                            display: true,
                            text: 'Nombre d\'avis par année'
                        },
                        legend: {display: false},
                    }
                }
            })
        })
    }


}
