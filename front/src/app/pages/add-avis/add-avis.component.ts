import {Component, OnInit, TemplateRef} from '@angular/core';
import {AvisService, checkAvisValide} from '../../services/avis.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {defaultsDeep} from 'lodash';
import {ConnectionService} from '../../services/connection.service';
import {Film} from '../../models/film.model';
import {FilmService} from '../../services/film.service';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Avis} from '../../models/avis.model';
import {UserService} from '../../services/user.service';
import {PopupComponent} from '../popup/popup.component';

@Component({
    selector: 'app-add-avis',
    templateUrl: './add-avis.component.html',
    styleUrls: ['./add-avis.component.css']
})

export class AddAvisComponent implements OnInit {
    films: Film[];
    noteSelected: number;
    filmSelected: Film;
    closeResult: string;
    userPoints: number;
    userName: string;
    id: string;
    onAddPage: boolean;
    avis: Avis
    userConnected = false
    addAvisValide: boolean

    constructor(
        private avisService: AvisService,
        private filmService: FilmService,
        private userService: UserService,
        private router: Router,
        private connectionService: ConnectionService,
        private route: ActivatedRoute,
        private modalService: NgbModal,
    ) {
    }

    // A l'initialisation de la page
    ngOnInit(): void {
        // On regarde si un user est connecté, on regarde si une id d'avis est renseignée.
        if (this.connectionService.user) {
            this.userConnected = true
        }
        this.id = this.route.snapshot.paramMap.get('id')
        this.onAddPage = !this.id
        // S'il n'y a pas d'id alors on redirige vers création avis sinon on recupère infos avis et on redirige vers modif
        if (this.onAddPage) {
            this.avis = new Avis()
        } else {
            this.avisService.getAvisById(parseInt(this.id, 10)).subscribe(a => {
                this.avis = a
                this.filmSelected = a.film
                this.noteSelected = a.note
            })
        }
        // on rafraichit les données de l'utilisateur pour obtenir l'actuel nombre de points
        this.connectionService.refreshUser().subscribe(u => console.log(u));
        this.filmService.getFilms().subscribe(films => this.films = films);
    }

    // Lors que le formulaire est soumis
    onSubmit(ngForm: NgForm) {
        // si on est sur une création d'avis, on regarde que le user n'a pas deja mis un avis sur le film
        // on envoie un nouvel avis au back
        if (this.onAddPage) {
            const checkAvis: checkAvisValide = {
                userId: this.connectionService.user.id.toString(),
                filmId: this.filmSelected.id.toString()
            }
            this.avisService.checkAddAvis(checkAvis).subscribe(bool => {
                this.addAvisValide = bool
                if (this.addAvisValide) {
                    const avis = defaultsDeep({
                        id: null,
                        user: this.connectionService.user,
                        film: this.filmSelected,
                        note: this.noteSelected,
                        texte: ngForm.form.value.texte,
                    });
                    this.avisService.addAvis(avis).subscribe(u => console.log(u));
                    this.connectionService.refreshUser().subscribe(u => console.log(u));
                    setTimeout(() => {
                    }, 1000);
                    // On ouvre la modal lorsque le user a plus de 2 points de fidélité et qu'il est autorisé à mettre un avis
                    if (this.connectionService.user.points >= 2 && this.addAvisValide) {
                        const modalRef = this.modalService.open(PopupComponent);
                        modalRef.componentInstance.name = this.connectionService.user.firstName;
                        // On reset les points de fidélité du user puisqu'il a donné plus de 2 avis et qu'il a profité d'une réduction
                        this.userService.addUser(this.connectionService.user).subscribe(u => console.log(u));
                    }
                    this.redirectListAvisCo();
                } else {
                    console.log('trop d\'avis')
                }
            })
            // Sinon on modifie les infos de l'avis et on envoie au back
        } else {
            const avis = defaultsDeep({
                id: this.avis.id,
                user: this.connectionService.user,
                film: this.filmSelected,
                note: this.noteSelected,
                texte: ngForm.form.value.texte,
            });
            this.avisService.updateAvis(avis).subscribe(f => console.log(avis));
            this.connectionService.refreshUser().subscribe(u => console.log(u));
            this.redirectListAvisCo();
        }
    }

    // redirige vers le listing des avis
    redirectListAvisCo() {
        setTimeout(() => {
            this.router.navigateByUrl('/list-avis/' + this.connectionService.user.id)
        }, 1000);
    }


}