import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {FilmService} from '../../services/film.service';
import {Film} from '../../models/film.model';
import { defaultsDeep } from 'lodash';

@Component({
    selector: 'app-add-film',
    templateUrl: './add-film.component.html',
    styleUrls: ['./add-film.component.css']
})
export class AddFilmComponent implements OnInit {

    constructor(private filmService: FilmService, private route: ActivatedRoute, private router: Router) {
    }

    id: string;
    onAddPage: boolean;
    film: Film
    // a l'initialisation, s'il y a une id on lance le formulaire de modif du film sinon le formulaire d'un nouveau film
    ngOnInit(): void {
        this.id = this.route.snapshot.paramMap.get('id')
        this.onAddPage = !this.id
        if(this.onAddPage){
            this.film = new Film()
        }
        else {
            this.filmService.getFilmById( parseInt(this.id,10 )).subscribe(f => {this.film = f
            console.log(this.film)})
        }
    }
    // à la soumission du formulaire on envoie le film au back
    onSubmit(ngForm: NgForm) {
        if(this.onAddPage) {
            const film = defaultsDeep({
                id: null,
                title: ngForm.form.value.title,
                director: ngForm.form.value.director,
                dateSortie: ngForm.form.value.dateSortie,
                synopsis: ngForm.form.value.synopsis,
            });
            this.filmService.addFilm(film).subscribe(f => console.log(film));
        }
        else {
            const film = defaultsDeep({
                id: this.film.id,
                title: ngForm.form.value.title,
                director: ngForm.form.value.director,
                dateSortie: ngForm.form.value.dateSortie,
                synopsis: ngForm.form.value.synopsis,
            });
            this.filmService.updateFilm(film).subscribe(f => console.log(film));
        }
         setTimeout(() => {
         this.router.navigateByUrl('/list-films')
         }, 1000);
    }


}


