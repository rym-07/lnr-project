import {Component, OnInit} from '@angular/core';
import {FilmService} from '../../services/film.service';
import {Film} from '../../models/film.model';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user.model';
import {Avis} from '../../models/avis.model';
import {AvisService} from '../../services/avis.service';

@Component({
    selector: 'app-panel-admin',
    templateUrl: './panel-admin.component.html',
    styleUrls: ['./panel-admin.component.css']
})
export class PanelAdminComponent implements OnInit {
    films: Film[];
    users: User[];
    avis: Avis[];
    average: number;

    constructor(private filmService: FilmService, private userService: UserService, private avisService: AvisService) {
    }

    ngOnInit(): void {
        this.filmService.getFilms().subscribe(films => this.films = films);
        this.userService.getUsers().subscribe(users => this.users = users);
        this.avisService.getAvis().subscribe(avis => this.avis = avis);
        this.filmService.getAverageFilms().subscribe(average => this.average = average);
    }

}
