import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {defaultsDeep} from 'lodash';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../models/user.model';
import {Film} from '../../models/film.model';

@Component({
    selector: 'app-add-user',
    templateUrl: './add-user.component.html',
    styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

    constructor(private userService: UserService, private router: Router, private route: ActivatedRoute) {
    }

    id: string;
    onAddPage: boolean;
    user: User;

    // a l'initialisation, s'il y a une id on lance le formulaire de modif du user sinon le formulaire d'un nouveau user
    ngOnInit() {
        this.id = this.route.snapshot.paramMap.get('id')
        this.onAddPage = !this.id
        if (this.onAddPage) {
            this.user = new User()
        } else {
            this.userService.getUserById(parseInt(this.id, 10)).subscribe(u => {
                this.user = u
                console.log(this.user)
            })
        }
    }

    // à la soumission du formulaire on envoie le user au back
    onSubmit(ngForm: NgForm) {
        if (this.onAddPage) {
            const user = defaultsDeep({
                id: null,
                firstName: ngForm.form.value.firstName,
                lastName: ngForm.form.value.lastName,
                email: ngForm.form.value.email,
                age: ngForm.form.value.age,
                points: 0,
            });
            this.userService.addUser(user).subscribe(u => console.log(u));
            setTimeout(() => {
                this.router.navigateByUrl('/')
            }, 1000);

        } else {
            const user = defaultsDeep({
                id: this.user.id,
                firstName: ngForm.form.value.firstName,
                lastName: ngForm.form.value.lastName,
                email: ngForm.form.value.email,
                age: ngForm.form.value.age,
                points: 0,
            });
            this.userService.updateUser(user).subscribe(u => console.log(u));
            setTimeout(() => {
                this.router.navigateByUrl('/user-page')
            }, 1000);
        }
    }
}
