import {Component, OnInit} from '@angular/core';
import {Film} from '../../models/film.model';
import {FilmService} from '../../services/film.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ExportCsvService} from '../../services/exportCsv.service';

@Component({
    selector: 'app-list-films',
    templateUrl: './list-films.component.html',
    styleUrls: ['./list-films.component.css']
})
export class ListFilmsComponent implements OnInit {

    films: Film[];
    id: string;

    constructor(private filmService: FilmService, private router: Router, private route: ActivatedRoute, private exportCsvService: ExportCsvService) {
    }

    ngOnInit() {
        this.filmService.getFilms().subscribe(films => this.films = films);
        this.id = this.route.snapshot.paramMap.get('id');
    }

    deleteFilm(id: number) {
        this.filmService.deleteFilm(id).subscribe(succes => {
            this.films = this.films.filter(film => film.id !== id)
        });
    }

    // on redirige vers l'ajout d'un fichier pour ajouter une photo
    redirectAddFile(id: number) {
        this.router.navigateByUrl('/add-file/' + id);
    }

    updateFilm(id: number) {
        this.router.navigateByUrl('/add-film/' + id);
    }

    // on appelle la methode qui download un fichier csv avec les bonnes valeurs
    downloadFile() {
        this.exportCsvService.downloadFile(this.films)
    }

}
