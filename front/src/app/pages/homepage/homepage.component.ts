import {Component, OnInit} from '@angular/core';
import {FilmService} from '../../services/film.service';
import {Film} from '../../models/film.model';
import {Router} from '@angular/router';
import {Avis} from '../../models/avis.model';
import {AvisService} from '../../services/avis.service';

@Component({
    selector: 'app-homepage',
    templateUrl: './homepage.component.html',
    styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

    constructor(private filmService: FilmService
        , private avisService: AvisService
        , private router: Router) {
    }

    films: Film[];
    avis: Avis[]

    // on affiche tous les films
    ngOnInit(): void {
        this.filmService.getFilms().subscribe(films => this.films = films);
    }

    redirectFilmAvis(id: number) {
        this.router.navigateByUrl('/avis-film/' + id);
    }
}


