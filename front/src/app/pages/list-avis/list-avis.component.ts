import {Component, OnInit} from '@angular/core';
import {Avis} from '../../models/avis.model';
import {AvisService} from '../../services/avis.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {ExportCsvService} from '../../services/exportCsv.service';

@Component({
    selector: 'app-list-avis',
    templateUrl: './list-avis.component.html',
    styleUrls: ['./list-avis.component.css']
})
export class ListAvisComponent implements OnInit {
    avis: Avis[];
    idUser: string
    onAdminPage: boolean;
    years: number[];
    yearSelected: string;

    constructor(private avisService: AvisService, private router: Router, private route: ActivatedRoute, private exportCsvService: ExportCsvService) {
    }

    ngOnInit() {
        this.avisService.findAllyears().subscribe(y => this.years = y)
        this.idUser = this.route.snapshot.paramMap.get('idUser')
        this.onAdminPage = !this.idUser
        // si on est sur la page admin on liste tous les avis, et ils ne peuvent pas être modifiés
        if (this.onAdminPage) {
            this.avisService.getAvis().subscribe(avis => {
                this.avis = avis
            });
            // si on vient d'un espace user, on liste les avis du user et il peut modifier ses avis
        } else {
            this.avisService.getAvisByUserId(this.idUser).subscribe(avisList => this.avis = avisList);
        }
    }

    deleteAvis(id: number) {
        this.avisService.deleteAvis(id).subscribe(succes => {
            this.avis = this.avis.filter(avis => avis.id !== id)
        });
    }

    updateAvis(id: number) {
        this.router.navigateByUrl('/add-avis/' + id);
    }

    // on appelle la methode qui download un fichier csv avec les bonnes valeurs
    downloadFile() {
        this.exportCsvService.downloadFile(this.avis)
    }

    // lorsque l'année selectionnée change on modifie les avis affichés
    valueSelectedChange(newValueYear) {
        this.yearSelected = newValueYear;
        this.avisService.findAvisByYear(newValueYear).subscribe(a => this.avis = a)

    }

}
