import { defaultsDeep } from 'lodash';
import {User} from './user.model';
import {Film} from './film.model';

export class Avis {
    id: number;
    note: number;
    dateAvis: Date;
    texte: string;
    user: User;
    film: Film;

    constructor(avis?: Partial<Avis>) {
        defaultsDeep(this, avis);
    }
}
