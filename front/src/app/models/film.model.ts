import { defaultsDeep } from 'lodash';

export class Film {
    id: number;
    title: string;
    dateSortie: Date;
    director: string;
    note: number;
    synopsis: string;
    image: string;

    constructor(film?: Partial<Film>) {
        defaultsDeep(this, film);
    }
}
