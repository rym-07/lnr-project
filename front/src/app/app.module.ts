import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListUsersComponent } from './pages/list-users/list-users.component';
import { AddUserComponent } from './pages/add-user/add-user.component';
import { HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {ListFilmsComponent} from './pages/list-films/list-films.component';
import { AddFilmComponent } from './pages/add-film/add-film.component';
import { ConnectionComponent } from './pages/connection/connection.component';
import { AddAvisComponent } from './pages/add-avis/add-avis.component';
import {ListAvisComponent} from './pages/list-avis/list-avis.component';
import { AddFileComponent } from './pages/add-file/add-file.component';
import { UserPageComponent } from './pages/user-page/user-page.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { PanelAdminComponent } from './pages/panel-admin/panel-admin.component';
import { ChartsComponent } from './pages/charts/charts.component';
import { AvisFilmComponent } from './pages/avis-film/avis-film.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PopupComponent } from './pages/popup/popup.component';



@NgModule({
  declarations: [
    AppComponent,
    ListUsersComponent,
    AddUserComponent,
    ListFilmsComponent,
    AddFilmComponent,
    AddFileComponent,
    ListAvisComponent,
    AddAvisComponent,
    AddFilmComponent,
    ConnectionComponent,
    AddAvisComponent,
    UserPageComponent,
    HomepageComponent,
    PanelAdminComponent,
    ChartsComponent,
    AvisFilmComponent,
    PopupComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
