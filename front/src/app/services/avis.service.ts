import {Injectable} from '@angular/core';
import {Film} from '../models/film.model';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {timeout} from 'rxjs/operators';
import {Avis} from '../models/avis.model';


@Injectable({
    providedIn: 'root'
})
export class AvisService {

    private url: string;

    constructor(private http: HttpClient) {
        this.url = environment.url;
    }

    getAvis(): Observable<Avis[]> {
        return this.http.get<Avis[]>(`${this.url}/avis`).pipe(timeout(10000));
    }

    addAvis(avis: Avis): Observable<Avis> {
        return this.http.post<any>(`${this.url}/avis`, avis).pipe(timeout(10000));
    }

    // retourne le nombre d'avis par mois en fonction d'une année selectionnée
    findNbAvisByYear(year: string): Observable<number[]> {
        return this.http.get<number[]>(`${this.url}/avis/${year}`).pipe(timeout(10000));
    }

    // retourne les avis d'une année précisée
    findAvisByYear(year: string): Observable<Avis[]> {
        return this.http.get<Avis[]>(`${this.url}/avis/findAvisBy/${year}`).pipe(timeout(10000));
    }

    updateAvis(avis: Avis): Observable<Avis> {
        return this.http.put<any>(`${this.url}/avis/avis`, avis).pipe(timeout(10000));
    }

    // retourne un avis par son id
    getAvisById(id: number): Observable<Avis> {
        return this.http.get<Avis>(`${this.url}/avis/this/${id}`).pipe(timeout(10000));
    }

    deleteAvis(id: number): Observable<any> {
        return this.http.delete(`${this.url}/avis/${id}`).pipe(timeout(10000));
    }

    // retourne tous les avis pour un film donné
    getAvisByFilm(id: string): Observable<Avis[]> {
        return this.http.get<Avis[]>(`${this.url}/avis/film/${id}`).pipe(timeout(10000));
    }

    // retourne tous les avis pour un user donné
    getAvisByUserId(userId: string): Observable<Avis[]> {
        return this.http.get<Avis[]>(`${this.url}/avis/user/${userId}`).pipe(timeout(10000));
    }

    // retourne vrai si le user n'a jamais mis d'avis sur un film faux sinon
    checkAddAvis(checkAvis: checkAvisValide): Observable<boolean> {
        return this.http.get<boolean>(`${this.url}/avis/checkAvis`, {params: {...checkAvis}}).pipe(timeout(10000));
    }

    // retourne toutes les années de publication d'avis
    findAllyears(): Observable<number[]> {
        return this.http.get<number[]>(`${this.url}/avis/findAllYears`).pipe(timeout(10000));
    }
}

export type checkAvisValide = {
    userId: string,
    filmId: string
};
