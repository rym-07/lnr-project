import {Injectable} from '@angular/core';
import {Film} from '../models/film.model';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {timeout} from 'rxjs/operators';
import {defaultsDeep} from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class FilmService {
    private url: string;

    constructor(private http: HttpClient) {
        this.url = environment.url;
    }

    getFilms(): Observable<Film[]> {
        return this.http.get<Film[]>(`${this.url}/films`).pipe(timeout(10000));
    }

    // retourne la moyenne des notes de tous les films
    getAverageFilms(): Observable<number> {
        return this.http.get<number>(`${this.url}/films/average`).pipe(timeout(10000));
    }

    getImage(id: number): Observable<File> {
        return this.http.get<File>(`${this.url}/films/images/${id}`).pipe(timeout(10000));
    }

    // ajout d'une image à un film
    addFile(filmCreateDto: FilmCreateDto): Observable<Film> {
        const data = new FormData();
        data.append('filmImage', filmCreateDto.filmImage)
        return this.http.post<any>(`${this.url}/films/${filmCreateDto.id}`, data).pipe(timeout(10000));
    }

    addFilm(film: Film): Observable<Film> {
        return this.http.post<any>(`${this.url}/films`, film).pipe(timeout(10000));
    }

    updateFilm(film: Film): Observable<Film> {
        return this.http.put<any>(`${this.url}/films/film`, film).pipe(timeout(10000));
    }

    getFilmById(id: number): Observable<Film> {
        return this.http.get<Film>(`${this.url}/films/film/${id}`).pipe(timeout(10000));
    }

    deleteFilm(id: number): Observable<any> {
        return this.http.delete(`${this.url}/films/${id}`).pipe(timeout(10000));
    }

    // supprime l'image d'un film
    deleteFile(id: string): Observable<any> {
        return this.http.post<any>(`${this.url}/films/deleteFile`, id).pipe(timeout(10000));
    }

    // retourne la liste des films pour un user donné
    getFilmsByUserId(userId: string): Observable<Film[]> {
        return this.http.get<Film[]>(`${this.url}/films/userfilms/${userId}`).pipe(timeout(10000));
    }
}

export type FilmCreateDto = {
    id: string,
    filmImage: File,
};