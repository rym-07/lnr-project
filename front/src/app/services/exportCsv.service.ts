import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ExportCsvService {
    constructor() {
    }

    // permet de sauvergarder un ficher avec les données affichées à l'écran
    downloadFile(data: any) {
        const replacer = (key, value) => (value === null ? '' : value); // specify how you want to handle null values here
        const header = Object.keys(data[0]);
        const csv = data.map((row) =>
            header
                .map((fieldName) => JSON.stringify(row[fieldName], replacer))
                .join(',')
        );
        csv.unshift(header.join(','));
        const csvArray = csv.join('\r\n');

        const a = document.createElement('a');
        const blob = new Blob([csvArray], {type: 'text/csv'});
        const url = window.URL.createObjectURL(blob);

        a.href = url;
        a.download = 'myFile.csv';
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
    }
}