import {Component, NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListUsersComponent} from './pages/list-users/list-users.component';
import {AddUserComponent} from './pages/add-user/add-user.component';
import {ListFilmsComponent} from './pages/list-films/list-films.component';
import {AddAvisComponent} from './pages/add-avis/add-avis.component';
import {AddFilmComponent} from './pages/add-film/add-film.component';
import {ConnectionComponent} from './pages/connection/connection.component';
import {HomepageComponent} from './pages/homepage/homepage.component';
import {AddFileComponent} from "./pages/add-file/add-file.component";
import {UserPageComponent} from "./pages/user-page/user-page.component";
import {PanelAdminComponent} from "./pages/panel-admin/panel-admin.component";
import {ChartsComponent} from "./pages/charts/charts.component";
import {ListAvisComponent} from './pages/list-avis/list-avis.component';
import {AvisFilmComponent} from './pages/avis-film/avis-film.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';



const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'list-users', component: ListUsersComponent },
  { path: 'add-user', component: AddUserComponent },
  { path: 'add-user/:id', component: AddUserComponent },
  { path: 'list-films', component: ListFilmsComponent },
  { path: 'add-film', component: AddFilmComponent },
  { path: 'add-film/:id', component: AddFilmComponent },
  { path: 'add-file/:id', component: AddFileComponent },
  { path: 'list-films', component: ListFilmsComponent },
  { path: 'add-avis', component: AddAvisComponent },
  { path: 'add-avis/:id', component: AddAvisComponent },
  { path: 'connection', component: ConnectionComponent },
  { path: 'panel-admin', component: PanelAdminComponent},
  { path: 'chart/:year', component: ChartsComponent},
  { path: 'user-page', component: UserPageComponent },
  { path: 'user-page/:idUser', component: UserPageComponent },
  { path: 'list-avis', component: ListAvisComponent },
  { path: 'list-avis/:idUser', component: ListAvisComponent },
  { path: 'avis-film/:id', component: AvisFilmComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
