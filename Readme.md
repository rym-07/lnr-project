**Lancer et faire tourner le projet:**

Dans un terminal bash, lancer la base de données avec Docker:

docker run --name mariadb --rm -e MYSQL_ROOT_PASSWORD=toor -e MYSQL_DATABASE=defaultdb -p 3306:3306 -v "`pwd`/initdb:/docker-entrypoint-initdb.d" mariadb


Pour lancer le front: dans un terminal bash et dans le dossier front, exécuter cette commande :

`ng serve`

Pour lancer le back: dans un terminal bash et dans le dossier api, exécuter ces commandes:

`mvn package`

`java -jar target/ciMood-0.0.1-SNAPSHOT.jar`

**Screenshot du projet**

_A l'eau l'EPF est l'appli pour tous les cinéphiles !_
- Découvre les dernières sorties au cinéma
- Note les films et donne ton avis
- Obtiens des points de fidélités pour aller dans un de nos cinémas partenaires

**Comment ça marche ?**
1. Inscris-toi ou connecte toi
2. Sur la homepage, retrouve tous les avis laissés par la communauté
3. Sur ton profil, tu peux ajouter un nouvel avis sur le dernier film que tu as vu, retrouver toutes tes informatoins ainsi que les films auxquels tu as laissé un avis
4. Au bout de 2 avis laissés, gagne 10% de remise chez nos cinémas partenaires
5. Sur le panel administrateur, tu peux avoir accès aux films, les créer et ajouter des images, accéder aux utilisateurs et avis de toute la communauté (CRUD), exporter les données en CSV et visualiser sous forme de stats les données et les filtrer.

_NB: tous les utilisateurs peuvent accéder aux panel admin car nous ne nous sommes pas focaliséés sur la sécurité de la connexion._


![Alt text](front/src/assets/Screenshot.png?raw=true "Screenshot")
